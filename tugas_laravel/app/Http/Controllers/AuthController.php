<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class AuthController extends Controller
{
    //
    public function daftar(){
        return view('halaman.biodata');
    }

    public function welcome(Request $request){
       $nama_depan = $request['firstname'];
       $nama_belakang= $request['lastname'];

       return view('halaman.home', ['nama_depan' => $nama_depan, 'nama_belakang' => $nama_belakang]);
    }
}
