<!DOCTYPE html>
<html>
<head>
	<title>Form HTML</title>
</head>
<body>
	<h1>Buat Account Baru!</h1>
	<h2>Sign Up Form</h2>
	<form action="/welcome" method="POST">
        @csrf
		<label>Full name:</label><br><br>
			<input type="text" name="firstname"><br><br>
			<label>Last name:</label><br><br>
			<input type="text" name="lastname"><br><br>
		
		<label>Gender:</label><br><br>
			<input type="radio" name="Gender" value="laki">Male <br>
			<input type="radio" name="Gender" value="perempuan">Female <br>
			<input type="radio" name="Gender" value="lainnya">Other <br><br>
		
		<label>Nationality:</label><br><br>
		<select name="Nationality">
			<option value="indonesian">Indonesia</option>
			<option value="singaporean">Singaporean</option>
			<option value="malaysian"> Malaysian</option>
			<option value="australian">Australia</option>
		</select><br><br>
		
		<label>Language Spoken:</label><br><br>
			<input type="checkbox" name="Bahasa" value="Indonesia">Bahasa Indonesia <br>
			<input type="checkbox" name="Bahasa" value="Inggris">English <br>
			<input type="checkbox" name="lainnya" value="lainnya">Other <br><br>
		
		<label>Bio:</label><br><br>
			<textarea name="message" rows="20" cols="30"></textarea><br>
		
		<input type="submit" value="Sign Up">
</form>
</body>
</html>